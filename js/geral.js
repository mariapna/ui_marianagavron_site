$(function(){


	/*****************************************
		SCRIPTS PÁGINA INICIAL
	*******************************************/
	//CARROSSEL DE DESTAQUE
	$("#carrosselDestaque").owlCarousel({
		items : 1,
        dots: true,
        loop: false,
        lazyLoad: true,
        mouseDrag:true,
        touchDrag  : true,	       
	    autoplayTimeout:5000,
	    autoplayHoverPause:true,
	    smartSpeed: 450,

	    //CARROSSEL RESPONSIVO
	    //responsiveClass:true,			    
 //        responsive:{
 //            320:{
 //                items:1
 //            },
 //            600:{
 //                items:2
 //            },
           
 //            991:{
 //                items:2
 //            },
 //            1024:{
 //                items:3
 //            },
 //            1440:{
 //                items:4
 //            },
            			            
 //        }		    		   		    
	    
	});

	

	//BOTÕES DO CARROSSEL
	var carrosselDestaque = $("#carrosselDestaque").data('owlCarousel');
	$('#flechaEsquerda').click(function(){ carrosselDestaque.prev(); });
	$('#flechaDireita').click(function(){ carrosselDestaque.next(); });

	$('.pg-portfolio .portfolio .btnAbrirMenuCategorias').click(function(){
		$('.pg-portfolio .portfolio .indice-mobile').addClass('abrir');
		$('body').addClass('stopScroll');
		$('.pg-portfolio .portfolio .lente').css('display','inline-block');
	});

	$('.pg-portfolio .portfolio .indice-mobile span').click(function(){
		$('.pg-portfolio .portfolio .indice-mobile').removeClass('abrir');
		$('body').removeClass('stopScroll');
		$('.pg-portfolio .portfolio .lente').css('display','none');
	});

	$('.pg-portfolio .portfolio .lente').click(function(){
		$('.pg-portfolio .portfolio .indice-mobile').removeClass('abrir');
		$('body').removeClass('stopScroll');
		$('.pg-portfolio .portfolio .lente').css('display','none');
	});
		
});